package com.gfs.gorgon.errorsextractors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.gfs.gorgon.GorgonError;

@Component("WebExchangeBindException")
public class WebExchangeBindExceptionErrorsExtractor implements ErrorsExtractor {

    @Override
    public List<GorgonError> extractErrors(Throwable throwable) {
        var webExchangeBindException = (WebExchangeBindException) throwable;
        List<GorgonError> errors = new ArrayList<>();
        for (FieldError fieldError : webExchangeBindException.getBindingResult().getFieldErrors()) {
            errors.add(GorgonError.builder()
                                  .message(fieldError.getDefaultMessage())
                                  .description(webExchangeBindException.getReason())
                                  .param(fieldError.getField())
                                  .build());
        }
        for (ObjectError objectError : webExchangeBindException.getBindingResult().getGlobalErrors()) {
            errors.add(GorgonError.builder()
                                  .message(objectError.getObjectName())
                                  .description(objectError.getDefaultMessage())
                                  .build());
        }
        return errors;
    }

    @Override
    public HttpStatus getHttpStatus(Throwable throwable) {
        return HttpStatus.BAD_REQUEST;
    }
}
