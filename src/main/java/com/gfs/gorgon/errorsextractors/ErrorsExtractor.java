package com.gfs.gorgon.errorsextractors;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.gfs.gorgon.GorgonError;

public interface ErrorsExtractor {

    List<GorgonError> extractErrors(Throwable throwable);

    HttpStatus getHttpStatus(Throwable throwable);
}
