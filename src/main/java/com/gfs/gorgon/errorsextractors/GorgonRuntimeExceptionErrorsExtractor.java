package com.gfs.gorgon.errorsextractors;

import java.util.List;

import com.gfs.gorgon.exceptions.GorgonRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.gfs.gorgon.GorgonError;
import com.gfs.gorgon.exceptions.GorgonException;

@Component("GorgonRuntimeException")
public class GorgonRuntimeExceptionErrorsExtractor implements ErrorsExtractor {

    @Override
    public List<GorgonError> extractErrors(Throwable throwable) {
        var gorgonRuntimeException = (GorgonRuntimeException) throwable;
        return List.of(GorgonError.builder()
                                  .message(gorgonRuntimeException.getErrorType().getMessage())
                                  .description(gorgonRuntimeException.getMessage())
                                  .build());
    }

    @Override
    public HttpStatus getHttpStatus(Throwable throwable) {
        var gorgonRuntimeException = (GorgonRuntimeException) throwable;
        return gorgonRuntimeException.getErrorType().getHttpStatus();
    }
}
