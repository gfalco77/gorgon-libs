package com.gfs.gorgon.exceptions;

import com.gfs.gorgon.ErrorType;

import lombok.Getter;

@Getter
public class GorgonRuntimeException extends RuntimeException {

    private final ErrorType errorType;

    public GorgonRuntimeException(ErrorType errorType, Object ...args) {
        super(String.format(errorType.getDescription(), args));
        this.errorType = errorType;
    }
}

