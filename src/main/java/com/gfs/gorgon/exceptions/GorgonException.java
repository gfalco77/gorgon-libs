package com.gfs.gorgon.exceptions;

import com.gfs.gorgon.ErrorType;

import lombok.Getter;

@Getter
public class GorgonException extends Exception {

    private final ErrorType errorType;

    public GorgonException(ErrorType errorType, Object ...args) {
        super(String.format(errorType.getDescription(), args));
        this.errorType = errorType;
    }
}

