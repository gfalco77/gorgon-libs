package com.gfs.gorgon;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.gfs.gorgon.errorsextractors.ErrorsExtractor;

import lombok.AllArgsConstructor;
import lombok.Getter;
import reactor.core.publisher.Mono;

@Component
@Order(-2)
public class GlobalErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {

    private static final String SUPPORT_MESSAGE = "Please contact us using the trace ID specified in the response.";
    private static final String TIMESTAMP = "timestamp";

    private final Map<String, ErrorsExtractor> stringExceptionMap;

    public GlobalErrorWebExceptionHandler(ErrorAttributes errorAttributes,
                                          WebProperties.Resources resources,
                                          ApplicationContext applicationContext,
                                          ServerCodecConfigurer configurer,
                                          Map<String, ErrorsExtractor> stringExceptionMap) {
        super(errorAttributes, resources, applicationContext);
        this.stringExceptionMap = stringExceptionMap;
        this.setMessageWriters(configurer.getWriters());
        this.setMessageReaders(configurer.getReaders());
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    private Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        var errorAttributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        var timestamp = (Date) errorAttributes.get(TIMESTAMP);
        var traceId = UUID.randomUUID().toString();
        var throwable = getError(request);

        var exceptionDetails = Optional.ofNullable(stringExceptionMap.get(throwable.getClass().getSimpleName()))
                                       .map(errorExtractors -> new ExceptionDetails(errorExtractors.getHttpStatus(throwable),
                                                                                    errorExtractors.extractErrors(throwable)))
                                       .orElseGet(() -> new ExceptionDetails(HttpStatus.INTERNAL_SERVER_ERROR,
                                                                             List.of(GorgonError.builder()
                                                                                                .message(NestedExceptionUtils.getMostSpecificCause(throwable)
                                                                                                                             .getMessage())
                                                                                                .build())));

        var errorResponse = ErrorResponse.builder()
                                         .traceId(traceId) // to be replaced
                                         .message(SUPPORT_MESSAGE)
                                         .errors(exceptionDetails.getErrors())
                                         .timestamp(timestamp)
                                         .httpStatus(exceptionDetails.getHttpStatus().value())
                                         .build();
        return ServerResponse.status(exceptionDetails.getHttpStatus()).bodyValue(errorResponse);
    }

    @AllArgsConstructor
    @Getter
    class ExceptionDetails {
        private HttpStatus httpStatus;
        private List<GorgonError> errors;
    }
}
