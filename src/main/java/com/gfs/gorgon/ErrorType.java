package com.gfs.gorgon;

import java.io.Serial;
import java.io.Serializable;

import org.springframework.http.HttpStatus;

public interface ErrorType extends Serializable {

    /**
     * HTTP status to set for the REST response
     *
     * @return Status
     */
    HttpStatus getHttpStatus();

    /**
     * The message to print to the end user
     *
     * @return message
     */
    String getMessage();

    /**
     * A more detailed message
     *
     * @return description
     */
    String getDescription();

}
